const express = require('express');
const admin = require('firebase-admin');
const bcrypt = require('bcrypt');
const path = require('path');
const mongoose = require("mongoose");

let staticPath = path.join(__dirname, "public");

const port = process.env.PORT || 4000;


let db = mongoose.connection;

db.on('error', () => console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to the cloud database.'))



mongoose.connect("mongodb+srv://admin:admin131@cluster0.ycdbp.mongodb.net/Capstone2?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//intializing express.js
const app = express();
app.use(express.json());

// hardcoding the products for now
const potProducts = [
    { id: 1, productName: "White and Pink Pot", price: 200 },
    { id: 2, productName: "Matte Blue Pot", price: 200 },
    { id: 3, productName: "Red and Black Pot", price: 200 }
];

const plantProducts = [
    { id: 4, productName: "Pothos", price: 200 },
    { id: 5, productName: "Fortune Plant", price: 700 },
    { id: 6, productName: "Selloum", price: 500 }
];

//middlewares
app.use(express.static(staticPath));

app.listen(3000, () => {
    console.log('listening on port 3000.......');
});

//routes
//home route
app.get("/", (req, res) => {
    res.sendFile(path.join(staticPath, "index.html"));
});

// 404 route
app.get('/404', (req, res) => {
    res.sendFile(path.join(staticPath, "404.html"));
});


//signup route
app.get('/signup', (req, res) => {
    res.sendFile(path.join(staticPath, "signup.html"));
});


//login route
app.get('/login', (req, res) => {
    res.sendFile(path.join(staticPath, "login.html"));
});

//products route
app.get('/products', (req, res) => {
    res.sendFile(path.join(staticPath, "products.html"));
});

app.get('/listproducts', (req, res) => {
    let { type } = req.query;

    if (type.toLowerCase() === "pots") {
        return res.json({ 'title': "Pots", 'data': potProducts });
    } else if (type.toLowerCase() === "plants") {
        return res.json({ 'title': "Plants", 'data': plantProducts })
    }
    return res.json({ 'unknown': "yes" });
});


const TokenSchema = new mongoose.Schema({
    token: String,
    // tokens expire after 
    // createdAt: { type: Date, expires: '10m', default: Date.now }
});


const UserSchema = new mongoose.Schema({
    name: { type: String, index: true, required: true, unique: true },
    email: { type: String, index: true, required: true, unique: true, lowercase: true },
    password: String,
    tac: Boolean,
    salt: String,
    notification: Boolean,
    sessionTokens: [TokenSchema]
});

const User = mongoose.model('mlg-users-0', UserSchema);
const Token = mongoose.model('mlg-tokens-0', TokenSchema);

app.post('/signup', (req, res) => {
    let { name, email, password, number, tac, notification } = req.body;

    // form validations
    if (name.length < 3) {
        return res.json({ 'alert': 'name must be 3 letters long' });
    } else if (!email.length) {
        return res.json({ 'alert': 'enter your email' });
    } else if (password.length < 8) {
        return res.json({ 'alert': 'password should be 8 letters long' });
    } else if (!number.length) {
        return res.json({ 'alert': 'enter your phone number' });
    } else if (!Number(number) || number.length < 10) {
        return res.json({ 'alert': 'invalid number, please enter valid one' });
    } else if (!tac) {
        return res.json({ 'alert': 'you must agree to our terms and conditions' });
    }

    bcrypt.genSalt(10, async (err, salt) => {
        bcrypt.hash(password, salt, async (err, hash) => {
            const newUser = new User({
                name: name,
                email: email,
                password: hash,
                salt: salt,
                tac: tac,
                notification: notification
            });

            const userExists = await User.exists({
                email: email
            });

            if (userExists) {
                return res.json({ 'alert': 'user already exists!' });
            }

            const sessionToken = require('crypto').randomBytes(64).toString('hex');

            const newToken = new Token({ token: sessionToken });

            console.log(newUser.sessionTokens.push(newToken));

            await newUser.save();

            return res.json({ 'success': 'login successful!', 'token': newToken.token, 'name': name, 'date': newToken.createdAt });
        })
    });
})


app.post('/login', async (req, res) => {
    let { email, password } = req.body;

    // login validations
    if (!email.length) {
        return res.json({ 'alert': 'wrong username format' });
    } else if (password.length < 8) {
        return res.json({ 'alert': 'password should be 8 letters long' });
    }

    const user = await User.findOne({
        email: email.toLowerCase()
    });

    if (!user) {

        return res.json({ 'alert': 'wrong username!' });
    }

    bcrypt.hash(password, user.salt, async (err, hash) => {
        if (!user.hash === hash) {
            return res.json({ 'alert': 'wrong password!' });
        }

        const sessionToken = require('crypto').randomBytes(64).toString('hex');

        const newToken = new Token({ token: sessionToken });

        console.log(user.sessionTokens.push(newToken));

        await user.save();

        return res.json({ 'success': 'login successful!', 'token': newToken.token, 'name': user.name, 'date': newToken.createdAt });
    });
})


app.use((req, res) => {
    res.redirect('/404');
});

app.listen(port, () => console.log(`Server running @ port: ${port}.`))

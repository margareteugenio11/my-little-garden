const createNav = () => {
    let nav = document.querySelector('.navbar');

    let userloginHtml = "";
    let loginoutHtml = `<button class="login-btn" onclick="location.href='/login';" >Login</button>`;

    if(IsJsonString(sessionStorage.user))
    {
        var userSessionData = JSON.parse(sessionStorage.user);

        if (userSessionData) {
            
            userloginHtml = `<a>${userSessionData.name}</a>`;
            loginoutHtml = `<button class="logout-btn" onclick="logOut();" >Logout</button>`;
        }
    }

    nav.innerHTML = `
    <div class="nav">
    <a href="/">
        <img src="img/logologo.png" class="brand-logo" alt="">
        </a>

    <div class="nav-items">
    <div class="search">
    <input type="text" class="search-box" placeholder="search pots, plants">
    <button class="search-btn">search</button>
    </div>
    <a href="#"><img src="img/user.png" alt=""></a>
    <a href="#"><img src="img/cart.png" alt=""></a>
    ${userloginHtml}
    ${loginoutHtml}

    </div>
    </div>
    
    `;
}

const logOut = () => {
    sessionStorage.clear();
    location.href="/";
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

createNav();
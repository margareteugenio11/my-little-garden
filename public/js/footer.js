const createFooter = () => {
    let footer = document.querySelector('footer');

    footer.innerHTML = `
    <div class="footer-content">
        
        </div>
    
    <div class="footer-social-container">
        
        <div>
            <a href="https://www.instagram.com/myhappylittlegarden/" class="social-link">instagram</a>
            <a href="https://www.facebook.com/MyHappyLittleGarden1/" class="social-link">facebook</a>
            <a href="#" class="social-link">twitter</a>
        </div>
    </div>
    <p class="footer-credit">Plants, Makes home clean and green</p>
    `;
}

createFooter();
const loader = document.querySelector('.loader');

// select inputs 
const submitBtn = document.querySelector('.submit-btn');
const name = document.querySelector('#name');
const email = document.querySelector('#email');
const password = document.querySelector('#password');
const number = document.querySelector('#number');
const tac = document.querySelector('#terms-and-cond');
const notification = document.querySelector('#notification');

submitBtn.addEventListener('click', () => {
    if (name.value.length < 3) {
        showAlert('name must be 3 letters long');
    } else if (!email.value.length) {
        showAlert('enter your email');
    } else if (password.value.length < 8) {
        showAlert('password should be 8 letters long');
    } else if (!number.value.length) {
        showAlert('enter your phone number');
    } else if (!Number(number.value) || number.value.length < 10) {
        showAlert('invalid number, please enter valid one');
    } else if (!tac.checked) {
        showAlert('you must agree to our terms and conditions');
    }
    else {
        // submit form
        loader.style.display = 'block';
        registerUser('/signup', {
            name: name.value,
            email: email.value,
            password: password.value,
            number: number.value,
            tac: tac.checked,
            notification: notification.checked,
            seller: false
        })
    }
})

// alert function
const showAlert = (msg, success) => {
    let alertBox = document.querySelector('.alert-box');

    let alertMsg = document.querySelector('.alert-msg');
    let alertImg = document.querySelector('.alert-img');

    let successMsg = document.querySelector('.success-msg');
    let successImg = document.querySelector('.success-img');

    if (success) {
        alertMsg.style.visibility = 'hidden';
        alertImg.style.visibility = 'hidden';
        successMsg.style.visibility = 'visible';
        successImg.style.visibility = 'visible';

        successMsg.innerHTML = msg;

    } else {

        alertMsg.style.visibility = 'visible';
        alertImg.style.visibility = 'visible';
        successMsg.style.visibility = 'hidden';
        successImg.style.visibility = 'hidden';

        alertMsg.innerHTML = msg;
    }

    alertBox.classList.add('show');

    setTimeout(() => {
        alertBox.classList.remove('show');
        if(success)
        {
            location.replace('/');
        }
    }, 3000);
}

// send data function
const registerUser = (path, data) => {
    fetch(path, {
        method: 'post',
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(data)
    }).then((res) => res.json())
        .then(response => {
            processData(response);
        })
}

const processData = (data) => {
    loader.style.display = null;
    if (data.alert) {
        showAlert(data.alert, false);
    } else if (data.success && data.token) {
        showAlert(data.success, true);

        // create authToken
        sessionStorage.user = JSON.stringify(data);
    }
}

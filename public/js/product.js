
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const productType = urlParams.get('type');


// send data function
const getProducts = () => {


    const esc = encodeURIComponent;
    const url = '/listproducts?';
    const params = {
        type: productType,
    };

    // this line takes the params object and builds the query string
    const query = Object.keys(params).map(k => `${esc(k)}=${esc(params[k])}`);


    fetch(url + query).then((res) => res.json())
        .then(response => {
            processProducts(response);
        })
}


getProducts();

const processProducts = (data) => {
    if (data.unknown || !data.title) {
        location.replace("/404");
    }

    const categoryH2 = document.querySelector(".product-category");

    categoryH2.innerHTML = data.title;

    const categoryContainer = document.querySelector(".product-container");

    data.data.forEach((item) => {
        categoryContainer.insertAdjacentHTML('beforeend',
            `<div class="product-card">
            <div class="product-image">
                  <img src="img/${item.id}.png" class="product-thumb" alt="">
                <button class="card-btn addcart${item.id}" onclick="addToCart(${item.id})">Add to Cart</button>
            </div>
            <div class="product-info">
                <h2 class="product-brand">${item.productName}</h2>
                <span class="price">₱${item.price}</span> 
            </div>
        </div>` );
    })
}

const addToCart = (id) => {

    const addCartButton = document.querySelector(`.addcart${id}`);

    addCartButton.innerHTML = "Added!"

    setTimeout(() => {
        addCartButton.innerHTML = "Add to Cart"
    }, 1000);


    var cartJson = window.sessionStorage.getItem("cart");

    if (!cartJson || !IsJsonString(cartJson)) {
        window.sessionStorage.setItem("cart", "[]");
    }

    var cart = JSON.parse(cartJson);
let selectedItem = null;
    cart.forEach(cartItem => {
        if (cartItem.prodId === id) {
            selectedItem = cartItem;
        }
    });


    if(!selectedItem)
    {
        selectedItem = { prodId : id, qty : 0};
        cart.push(selectedItem);
    }

    selectedItem.qty += 1;

    var cartStr = JSON.stringify(cart);
    
    window.sessionStorage.setItem("cart", cartStr);


}



function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

